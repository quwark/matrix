package test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        int n = 1;

        switch (n) {
            case 1:
                task1();
                break;
            case 2:
                task2();
                break;
            case 3:
                task3();
                break;
            case 4:
                task4();
                break;
            case 5:
                task5();
                break;
        }
    }

    private static void task1() {
        System.out.println("Input Matrix A Size:");
        int a = readInt();

        double[][] g = new double[a][a];
        readMatrix(g);

        displayMatrix(g);
        System.out.println("");
        transp(g);

        System.out.println("Result:");
        displayMatrix(g);
    }

    private static void task2() {
        System.out.println("Input Matrix Size:");
        int a = readInt();
        int b = readInt();

        double[][] g = new double[a][b];
        double[][] h = new double[a][b];

        readMatrix(g);
        readMatrix(h);

        displayMatrix(g);
        System.out.println("");
        displayMatrix(h);
        System.out.println("");

        System.out.println("Result:");
        displayMatrix(sum(g, h));
    }

    private static void task3() {
        System.out.println("Input Matrix A Size:");
        int a = readInt();
        int b = readInt();
        System.out.println("Input Matrix B Size:");
        int b2 = readInt();
        int c = readInt();

        if(b != b2) {
            return;
        }

        double[][] g = new double[a][b];
        double[][] h = new double[b][c];

        readMatrix(g);
        readMatrix(h);

        displayMatrix(g);
        System.out.println("");
        displayMatrix(h);
        System.out.println("");

        System.out.println("Result:");
        displayMatrix(mult(g, h));
    }

    private static void task4() {
        System.out.println("Input Matrix A Size:");
        int a = readInt();
        int b = readInt();
        System.out.println("Input Matrix B Size:");
        int b2 = readInt();
        int c = readInt();

        if(b != b2) {
            return;
        }

        double[][] g = new double[a][b];
        double[][] h = new double[b][c];

        readMatrix(g);
        readMatrix(h);

        displayMatrix(g);
        System.out.println("");
        displayMatrix(h);
        System.out.println("");

        System.out.println("Result:");
        displayMatrix(multVinograd(g, h));
    }



    private static void task5(){
        System.out.println("Input Matrix A Size:");
        int a = readInt();
        int b = readInt();
        System.out.println("Input Matrix B Size:");
        int b2 = readInt();
        int c = readInt();

        if(b != b2) {
            return;
        }

        double[][] g = new double[a][b];
        double[][] h = new double[b][c];

        readMatrix(g);
        readMatrix(h);

        displayMatrix(g);
        System.out.println("");
        displayMatrix(h);
        System.out.println("");

        int dim = getNewDimension(g, h);

        double[][] g_n = extendMatrixToSquare2(g, dim);
        double[][] h_n = extendMatrixToSquare2(h, dim);

        double[][] r = multiStrassen(g_n, h_n, dim);

        System.out.println("Result:");
        displayMatrix(cutMatrix(r, a, c));

    }

    private static void transp(double [][] a) {
        int n = a.length;
        double tmp;
        
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                tmp = a[i][j];
                a[i][j] = a[j][i];
                a[j][i] = tmp;
            }
        }
    }

    private static void readMatrix(double[][] matrix) {
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.println("Input element [" + i + "][" + j + "]:");
                matrix[i][j] = scanner.nextDouble();
            }
        }
    }

    private static int readInt(){
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    private static double[][] cutMatrix(double[][] a, int rows, int columns) {
        double[][] result = new double[rows][columns];

        for (int i = 0; i < rows; i++) {
            System.arraycopy(a[i], 0, result[i], 0, columns);
        }

        return result;
    }

    private static void displayMatrix(double[][] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                System.out.printf("%5.1f", a[i][j]);
            }
            System.out.println("");
        }
    }

    private static int log2(int x) {
        int result = 1;
        while((x >>= 1) != 0) {
            result++;
        }
        return result;
    }


    private static int getNewDimension(double[][] a, double[][] b) {
        return 1 << log2(Collections.max(Arrays.asList(a.length, a[0].length, b[0].length)));
    }

    private static double[][] extendMatrixToSquare2(double[][] a, int n) {
        double[][] result = new double[n][n];

        for (int i = 0; i < a.length; i++) {
            System.arraycopy(a[i], 0, result[i], 0, a[i].length);
        }
        return result;
    }

    private static void splitMatrix(double[][] a, double[][] a11, double[][] a12, double[][] a21, double[][] a22) {
        int n = a.length >> 1;

        for (int i = 0; i < n; i++) {
            System.arraycopy(a[i], 0, a11[i], 0, n);
            System.arraycopy(a[i], n, a12[i], 0, n);
            System.arraycopy(a[n + i], 0, a21[i], 0, n);
            System.arraycopy(a[n + i], n, a22[i], 0, n);
        }
    }

    private static double[][] collectMatrix(double[][] a11, double[][] a12, double[][] a21, double[][] a22) {
        int n = a11.length;
        double[][] a = new double[n << 1][n << 1];

        for (int i = 0; i < n; i++) {
            System.arraycopy(a11[i], 0, a[i], 0, n);
            System.arraycopy(a12[i], 0, a[i], n, n);
            System.arraycopy(a21[i], 0, a[n + i], 0, n);
            System.arraycopy(a22[i], 0, a[n + i], n, n);
        }
        return a;
    }

    private static double[][] sum(double[][] a, double[][] b) {
        double[][] result = new double[a.length][a[0].length];

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                result[i][j] = a[i][j] + b[i][j];
            }
        }
        return result;
    }

    private static double[][] sub(double[][] a, double[][] b) {
        double[][] result = new double[a.length][a[0].length];

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                result[i][j] = a[i][j] - b[i][j];
            }
        }
        return result;
    }


    private static double[][] mult(double[][] a, double[][] b) {
        int rows_a = a.length;
        int columns_b = b[0].length;
        int columnsA_rowsB = a[0].length;

        double[][] result = new double[rows_a][columns_b];


        for (int i = 0; i < rows_a; i++) {
            for (int j = 0; j < columns_b; j++) {
                double sum = 0;
                for (int k = 0; k < columnsA_rowsB; k++) {
                    sum += a[i][k] * b[k][j];
                }
                result[i][j] = sum;
            }
        }
        return result;
    }

    private static double[][] multiStrassen(double[][] a, double[][] b, int n) {
        if(n <= 4) {
            return mult(a, b);
        }

        n = n >> 1;

        double[][] a11 = new double[n][n];
        double[][] a12 = new double[n][n];
        double[][] a21 = new double[n][n];
        double[][] a22 = new double[n][n];

        double[][] b11 = new double[n][n];
        double[][] b12 = new double[n][n];
        double[][] b21 = new double[n][n];
        double[][] b22 = new double[n][n];

        splitMatrix(a, a11, a12, a21, a22);
        splitMatrix(b, b11, b12, b21, b22);

        double[][] p1 = multiStrassen(sum(a11, a22), sum(b11, b22), n);
        double[][] p2 = multiStrassen(sum(a21, a22), b11, n);
        double[][] p3 = multiStrassen(a11, sub(b12, b22), n);
        double[][] p4 = multiStrassen(a22, sub(b21, b11), n);
        double[][] p5 = multiStrassen(sum(a11, a12), b22, n);
        double[][] p6 = multiStrassen(sub(a21, a11), sum(b11, b12), n);
        double[][] p7 = multiStrassen(sub(a12, a22), sum(b21, b22), n);

        double[][] c11 = sum(sum(p1, p4), sub(p7, p5));
        double[][] c12 = sum(p3, p5);
        double[][] c21 = sum(p2, p4);
        double[][] c22 = sum(sub(p1, p2), sum(p3, p6));

        return collectMatrix(c11, c12, c21, c22);
    }


    private static double[][] multVinograd(double[][] g, double[][] h) {

        int a = g.length;
        int b = h.length;
        int c = h[0].length;

        double[][] r = new double[a][c];


        int d = b / 2;

        double[] rowFactor = new double[a];
        double[] coluimnFactor = new double[c];

        for (int i = 0; i < a; i++) {
            rowFactor[i] = g[i][0] * g[i][1];
            for (int j = 1; j < d; j++) {
                rowFactor[i] += g[i][2 * j] * g[i][2 * j + 1];
            }
        }

        for (int i = 0; i < c; i++) {
            coluimnFactor[i] = h[0][i] * h[1][i];
            for (int j = 1; j < d; j++) {
                coluimnFactor[i] += h[2 * j][i] * h[2 * j + 1][i];
            }
        }

        for (int i = 0; i < a; i++) {
            for (int j = 0; j < c; j++) {
                r[i][j] = -rowFactor[i] - coluimnFactor[j];
                for (int k = 0; k < d; k++) {
                    r[i][j] += (g[i][2 * k] + h[2 * k + 1][j]) * (g[i][2 * k + 1] + h[2 * k][j]);
                }
            }
        }

        if(2 * (b / 2) != b) {
            for (int i = 0; i < a; i++) {
                for (int j = 0; j < c; j++) {
                    r[i][j] += g[i][b - 1] * h[b - 1][j];
                }
            }
        }

        return r;
    }

}
